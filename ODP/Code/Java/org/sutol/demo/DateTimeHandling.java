package org.sutol.demo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.openntf.domino.DateTime;
import org.openntf.domino.Session;
import org.openntf.domino.utils.Factory;

public class DateTimeHandling implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final String COLOR_AFTER = "danger";
	private static final String COLOR_BEFORE = "success";
	private static final String COLOR_EQUAL = "info";

	public List<DateLine> getDates() {
		List<DateLine> list = new ArrayList<DateLine>();
		Session session = Factory.getSession();

		DateTime start1 = session.createDateTime(2015, 11, 11, 0, 0, 0);
		DateTime end1 = session.createDateTime(2015, 11, 11, 12, 0, 0);
		list.add(new DateLine(start1, end1));

		DateTime start2 = session.createDateTime(2015, 11, 12, 0, 0, 0);
		DateTime end2 = session.createDateTime(2015, 11, 11, 12, 0, 0);
		list.add(new DateLine(start2, end2));

		DateTime start3 = session.createDateTime(2015, 11, 12, 0, 0, 0);
		DateTime end3 = session.createDateTime(2015, 11, 12, 0, 0, 0);
		list.add(new DateLine(start3, end3));

		return list;
	}

	class DateLine {
		private DateTime start;
		private DateTime end;

		public DateLine(DateTime start, DateTime end) {
			this.start = start;
			this.end = end;
		}

		public DateTime getStart() {
			return start;
		}

		public DateTime getEnd() {
			return end;
		}

		public String getResult() {
			String result = "=";
			if (getStart().isAfter(getEnd())) {
				result = ">";
			}
			if (getStart().isBefore(getEnd())) {
				result = "<";
			}

			return result;
		}

		public String getColor() {
			String color = COLOR_EQUAL;
			if (getStart().isAfter(getEnd())) {
				color = COLOR_AFTER;
			}
			if (getStart().isBefore(getEnd())) {
				color = COLOR_BEFORE;
			}
			return color;
		}
	}

}
