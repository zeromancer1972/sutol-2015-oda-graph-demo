package org.sutol.demo.xots;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.Callable;

import org.openntf.domino.xots.Tasklet;

@Tasklet(session = Tasklet.Session.CLONE, schedule="* * * * *")
public class PeriodicTask implements Callable<String> {

	public String call() {
		Calendar now = Calendar.getInstance();
		SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy, HH:mm:ss");
		String result = "Message from PeriodicTask: " + formatter.format(now.getTime());
		createOutput(result);
		return result;
	}

	private void createOutput(String msg) {
		System.out.println(msg);
	}

}
