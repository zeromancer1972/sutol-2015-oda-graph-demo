package org.sutol.demo.xots;

import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import javax.faces.context.FacesContext;

import org.openntf.domino.thread.DominoExecutor;
import org.openntf.domino.thread.PeriodicScheduler;
import org.openntf.domino.utils.Factory;
import org.openntf.domino.utils.Factory.SessionType;
import org.openntf.domino.xots.Xots;
import org.openntf.domino.xsp.XspOpenLogUtil;

import com.ibm.xsp.extlib.util.ExtLibUtil;

public class XotsDemo {
	private static final long serialVersionUID = 1L;

	public static void testSessionCallable() {
		startCheck();
		try {
			Future<String> future = Xots.getService().submit(new SessionCallable());
			createMessage(future.get(), "xotsSessionCallable");
		} catch (Throwable t) {
			XspOpenLogUtil.logError(t);
			createMessage(t.getMessage(), "xotsSessionCallable");
		}
	}
	
	public static void testSessionCallableFacesContext() {
		startCheck();
		try {
			Future<String> future = Xots.getService().submit(new SessionCallable(FacesContext.getCurrentInstance()));
			createMessage(future.get(), "xotsSessionCallable");
		} catch (Throwable t) {
			XspOpenLogUtil.logError(t);
			createMessage(t.getMessage(), "xotsSessionCallable");
		}
	}
	
	public static void testSessionCallableSession() {
		startCheck();
		try {
			Future<String> future = Xots.getService().submit(new SessionCallable(Factory.getSession(SessionType.CURRENT)));
			createMessage(future.get(), "xotsSessionCallable");
		} catch (Throwable t) {
			XspOpenLogUtil.logError(t);
			createMessage(t.getMessage(), "xotsSessionCallable");
		}
	}

	public static void startPeriodicTasklet() {
		startCheck();
		PeriodicScheduler sched = new PeriodicScheduler(0, 5, TimeUnit.SECONDS);
		Xots.getService().schedule(new PeriodicTask(), sched);
	}

	public static void stopPeriodicTasklet() {
		Xots.stop(0);
	}

	private static void startCheck() {
		if (!Xots.isStarted()) {
			DominoExecutor executor = new DominoExecutor(10);
			Xots.start(executor);
		}
	}

	private static void createMessage(String msg, String scope) {
		ExtLibUtil.getViewScope().put(scope, msg);
		XspOpenLogUtil.logEvent(null, msg, Level.INFO, null);
	}
}
