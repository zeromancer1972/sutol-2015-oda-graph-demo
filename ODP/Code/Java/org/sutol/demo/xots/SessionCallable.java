package org.sutol.demo.xots;

import java.util.concurrent.Callable;

import javax.faces.context.FacesContext;

import org.openntf.domino.Session;
import org.openntf.domino.utils.Factory;
import org.openntf.domino.utils.Factory.SessionType;
import org.openntf.domino.xots.Tasklet;

//clone the session of the current user (like "run as web user")
@Tasklet(session = Tasklet.Session.CLONE)
public class SessionCallable implements Callable<String> {

	private FacesContext context;
	private String userName;

	public SessionCallable() {
		// empty constructor	
	}

	public SessionCallable(FacesContext ctx) {
		this.context = ctx;
	}

	public SessionCallable(Session session) {
		userName = session.getEffectiveUserName();
	}

	public String call() throws Exception {
		try {
			if (this.context != null) {
				return "FacesContext: " + context.toString();
			} else if (userName != null) {
				return "Session.getEffectiveUserName: " + this.userName;
			} else {
				return "Return value from the tasklet: " + Factory.getSession(SessionType.CURRENT).getEffectiveUserName();
			}
		} catch (Throwable t) {
			t.printStackTrace();
			return t.getMessage();
		}
	}

}
