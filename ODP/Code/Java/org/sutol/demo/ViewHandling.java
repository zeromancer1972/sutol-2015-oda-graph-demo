package org.sutol.demo;

import java.io.Serializable;
import java.util.Date;
import java.util.logging.Level;

import lotus.domino.NotesException;

import org.openntf.domino.Database;
import org.openntf.domino.Document;
import org.openntf.domino.DocumentCollection;
import org.openntf.domino.View;
import org.openntf.domino.utils.Factory;
import org.openntf.domino.xsp.XspOpenLogUtil;

import com.ibm.xsp.extlib.util.ExtLibUtil;

public class ViewHandling implements Serializable{

	private static final long serialVersionUID = 1L;
	private static final String VIEW_NAME = "AllContacts";

	public void loopAllDocuments() {
		XspOpenLogUtil.logEvent(null, "loopAllDocuments", Level.INFO, null);
		int count = 0;
		long startTime = System.nanoTime();
		try {
			lotus.domino.Session session = ExtLibUtil.getCurrentSession();
			lotus.domino.Database db = session.getCurrentDatabase();
			lotus.domino.DocumentCollection col = db.getAllDocuments();
			lotus.domino.Document doc = col.getFirstDocument();
			lotus.domino.Document tmp = null;
			while (doc != null) {
				tmp = col.getNextDocument(doc);
				// do sth. with the doc
				@SuppressWarnings("unused")
				String form = doc.getItemValueString("form");
				count++;
				doc.recycle();
				doc = tmp;
			}
			col.recycle();
			db.recycle();
			System.gc();

		} catch (NotesException e) {
			setError("loop1", e);
			XspOpenLogUtil.logError(e);
		}
		long endTime = System.nanoTime();
		finished("loop1", "Old Java Looped though " + count + " docs", startTime, endTime);
	}

	public void loopAllDocumentsODA() {
		XspOpenLogUtil.logEvent(null, "loopAllDocumentsODA", Level.INFO, null);
		int count = 0;
		long startTime = System.nanoTime();

		Database db = Factory.getSession().getCurrentDatabase();
		DocumentCollection col = db.getAllDocuments();
		for (Document doc : col) {
			// do sth. with the doc
			@SuppressWarnings("unused")
			String form = doc.getItemValueString("form");
			count++;
		}

		long endTime = System.nanoTime();
		finished("loop2", "ODA Looped though " + count + " docs", startTime, endTime);
	}

	public void loopAndModify() {
		XspOpenLogUtil.logEvent(null, "loopAndModify", Level.INFO, null);
		int count = 0;
		long startTime = System.nanoTime();
		try {
			lotus.domino.Session session = ExtLibUtil.getCurrentSession();
			lotus.domino.Database db = session.getCurrentDatabase();
			lotus.domino.View v = db.getView(VIEW_NAME);
			lotus.domino.Document doc = v.getFirstDocument();
			lotus.domino.Document tmp = null;
			while (doc != null) {
				tmp = v.getNextDocument(doc);
				// do sth. with the doc
				doc.replaceItemValue("mod", session.createDateTime(new Date()));
				doc.save();
				count++;
				doc.recycle();
				doc = tmp;
			}
			v.recycle();
			db.recycle();
			System.gc();

		} catch (NotesException e) {
			setError("loop1", e);
			XspOpenLogUtil.logError(e);
		}
		long endTime = System.nanoTime();
		finished("loop3", "Modified " + count + " docs", startTime, endTime);
	}

	public void loopAndModifyODA() {
		XspOpenLogUtil.logEvent(null, "loopAndModifyODA", Level.INFO, null);
		int count = 0;
		long startTime = System.nanoTime();

		Database db = Factory.getSession().getCurrentDatabase();
		View v = db.getView(VIEW_NAME);
		
		for (Document doc : v.getAllDocuments()) {
			// do sth. with the doc
			doc.replaceItemValue("mode", new Date());
			doc.save();
			count++;
		}

		long endTime = System.nanoTime();
		finished("loop4", "Modified " + count + " docs", startTime, endTime);
	}

	private void finished(String name, String msg, long startTime, long endTime) {
		ExtLibUtil.getViewScope().put(name, msg + ". Finished in " + (endTime - startTime) / 1000000 + " ms");
	}

	private void setError(String name, Throwable e) {
		ExtLibUtil.getViewScope().put(name, e.getMessage());
	}
}
