package org.sutol.demo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;
import java.util.logging.Level;

import org.openntf.domino.Database;
import org.openntf.domino.Document;
import org.openntf.domino.DocumentCollection;
import org.openntf.domino.utils.Factory;
import org.openntf.domino.xsp.XspOpenLogUtil;

public class DocumentHandling implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final String VIEW_NAME = "custom";

	public void createDocumentWithPairs() {
		XspOpenLogUtil.logEvent(null, "createDocumentWithPairs", Level.INFO, null);
		Database db = Factory.getSession().getCurrentDatabase();
		Document doc = db.createDocument("form", "custom", "id", UUID.randomUUID(), "date", Factory.getSession().createDateTime(new Date()));
		doc.save();
	}

	public void createDocumentWithMap() {
		XspOpenLogUtil.logEvent(null, "createDocumentWithMap", Level.INFO, null);
		Database db = Factory.getSession().getCurrentDatabase();
		HashMap<String, Object> fieldsMap = new HashMap<String, Object>();
		fieldsMap.put("form", "custom");
		fieldsMap.put("id", UUID.randomUUID());
		Document doc = db.createDocument(fieldsMap);
		// add a date via put method, not as map value!
		doc.put("date", new Date());
		doc.save();
	}

	public DocumentCollection getDocuments() {
		Database db = Factory.getSession().getCurrentDatabase();
		return db.getView(VIEW_NAME).getAllDocuments();
	}

	public void remove(Document doc) {
		doc.remove(true);
	}

	public void getIllegalItem(Document doc, String itemName) {
		try {
			ArrayList<?> values = doc.getItemValue(itemName, ArrayList.class);
			if (values != null) {
				XspOpenLogUtil.logEvent(null, "item \"" + itemName + "\", size of item: " + values.size() + ", type: " + values.get(0).getClass().toString(), Level.INFO, null);
			} else {
				XspOpenLogUtil.logEvent(null, "item \"" + itemName + "\" does not exist", Level.INFO, null);
			}

		} catch (Throwable e) {
			XspOpenLogUtil.logError(null, e, "", Level.SEVERE, null);
		}
	}

}
