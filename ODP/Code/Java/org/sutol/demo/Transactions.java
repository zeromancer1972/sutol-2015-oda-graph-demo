package org.sutol.demo;

import java.io.Serializable;
import java.util.Date;
import java.util.logging.Level;

import org.openntf.domino.Database;
import org.openntf.domino.Document;
import org.openntf.domino.DocumentCollection;
import org.openntf.domino.Session;
import org.openntf.domino.View;
import org.openntf.domino.transactions.DatabaseTransaction;
import org.openntf.domino.utils.Factory;
import org.openntf.domino.xsp.XspOpenLogUtil;

import com.ibm.xsp.extlib.util.ExtLibUtil;

public class Transactions implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final String VIEW_NAME = "AllContacts";

	public void modifyDocuments(boolean successOrFail) {
		XspOpenLogUtil.logEvent(null, "Start modifying document " + (successOrFail ? "with" : "without") + " success.", Level.INFO, null);

		Session session = Factory.getSession();
		Database db = session.getCurrentDatabase();
		DatabaseTransaction txn = db.startTransaction();
		View v = db.getView(VIEW_NAME);
		DocumentCollection col = v.getAllDocuments();
		int count = 0;
		for (Document doc : col) {
			count++;
			if (count > 20) {
				// we stop after 20 documents to keep the demo fast
				break;
			}
			doc.replaceItemValue("txnTest", new Date());
		}
		count--;
		if (successOrFail) {
			txn.commit();
			createMessage("<i class=\"fa fa-check\"></i> Changes comitted. (" + count + ")");
		} else {
			txn.rollback();
			createMessage("<i class=\"fa fa-exclamation-triangle\"></i> Rolling back... (" + count + ")");
		}
	}

	public DocumentCollection getDocuments() {
		Session session = Factory.getSession();
		Database db = session.getCurrentDatabase();
		View v = db.getView(VIEW_NAME);
		DocumentCollection col = v.getAllDocuments();
		return col;
	}

	private void createMessage(String msg) {
		ExtLibUtil.getViewScope().put("transResult", msg);
		XspOpenLogUtil.logEvent(null, msg, Level.INFO, null);
	}

}
