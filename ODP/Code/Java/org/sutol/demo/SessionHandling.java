package org.sutol.demo;

import java.io.Serializable;

import lotus.domino.NotesException;
import lotus.domino.Session;

import org.openntf.domino.utils.Factory;
import org.openntf.domino.utils.XSPUtil;
import org.openntf.domino.utils.Factory.SessionType;

import com.ibm.xsp.extlib.util.ExtLibUtil;

public class SessionHandling implements Serializable {
	private static final long serialVersionUID = 1L;

	public String userCurrent() {
		return Factory.getSession().getEffectiveUserName();
	}

	public String userSigner() {
		return Factory.getSession(SessionType.SIGNER).getEffectiveUserName();
	}
	
	public String userXspSession(){
		return XSPUtil.getCurrentSession().getEffectiveUserName();
	}
	
	public String userResolveSession() throws NotesException{
		Session session = (Session) ExtLibUtil.resolveVariable("session");
		return session.getEffectiveUserName();
	}
	
	public String userResolveSessionAsSigner() throws NotesException{
		Session session = (Session) ExtLibUtil.resolveVariable("sessionAsSigner");
		return session.getEffectiveUserName();
	}

	public String extlibUser() throws NotesException {
		return ExtLibUtil.getCurrentSession().getEffectiveUserName();
	}

	public String xsputilUser() {
		return XSPUtil.getCurrentSession().getEffectiveUserName();
	}
}
