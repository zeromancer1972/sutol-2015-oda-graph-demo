package org.sutol.demo.graph;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;

import org.openntf.domino.Database;
import org.openntf.domino.Document;
import org.openntf.domino.DocumentCollection;
import org.openntf.domino.Name;
import org.openntf.domino.Session;
import org.openntf.domino.graph2.impl.DConfiguration;
import org.openntf.domino.graph2.impl.DElementStore;
import org.openntf.domino.graph2.impl.DFramedGraphFactory;
import org.openntf.domino.graph2.impl.DFramedTransactionalGraph;
import org.openntf.domino.graph2.impl.DGraph;
import org.openntf.domino.utils.Factory;
import org.openntf.domino.utils.Factory.SessionType;
import org.openntf.domino.xsp.XspOpenLogUtil;

import com.ibm.xsp.extlib.util.ExtLibUtil;

public class GraphController {
	private static StringBuilder messages = new StringBuilder();

	public static DFramedTransactionalGraph<DGraph> setupGraph() {

		try {
			DElementStore sessionStore = new DElementStore();
			sessionStore.setStoreKey(GraphConfig.SESSIONS_PATH);
			sessionStore.addType(ConferenceSession.class);

			DElementStore attendeeStore = new DElementStore();
			attendeeStore.setStoreKey(GraphConfig.ATTENDEES_PATH);
			attendeeStore.addType(Attendee.class);

			DElementStore presenterStore = new DElementStore();
			presenterStore.setStoreKey(GraphConfig.PRESENTER_PATH);
			presenterStore.addType(Presenter.class);

			DConfiguration config = new DConfiguration();
			DGraph graph = new DGraph(config);

			config.addElementStore(sessionStore);
			config.addElementStore(attendeeStore);
			config.addElementStore(presenterStore);

			config.setDefaultElementStore(sessionStore.getStoreKey());

			DFramedGraphFactory factory = new DFramedGraphFactory(config);
			DFramedTransactionalGraph<DGraph> fg = (DFramedTransactionalGraph<DGraph>) factory.create(graph);
			return fg;
		} catch (Throwable t) {
			XspOpenLogUtil.logError(t);
			return null;
		}
	}

	public static void run() {
		initDbs();
		buildGraph();
		testGraph("graphMessages");
	}

	/**
	 * aux method to reset all data in the graph store
	 */
	public static void initDbs() {
		Session session = Factory.getSession(SessionType.CURRENT);
		Database sessionDb = session.getDatabase(GraphConfig.SESSIONS_PATH);
		Database presenterDb = session.getDatabase(GraphConfig.PRESENTER_PATH);
		Database attendeeDb = session.getDatabase(GraphConfig.ATTENDEES_PATH);

		if (sessionDb != null) {
			removeDocs(sessionDb.getAllDocuments());
			XspOpenLogUtil.logEvent(null, "Removed all sessions", Level.INFO, null);
		}
		if (presenterDb != null) {
			removeDocs(presenterDb.getAllDocuments());
			XspOpenLogUtil.logEvent(null, "Removed all presenter", Level.INFO, null);
		}
		if (attendeeDb != null) {
			removeDocs(attendeeDb.getAllDocuments());
			XspOpenLogUtil.logEvent(null, "Removed all attendees", Level.INFO, null);
		}

	}

	private static void removeDocs(DocumentCollection col) {
		for (Document doc : col) {
			doc.remove(true);
		}
	}

	public static void createMessage(String msg) {
		XspOpenLogUtil.logEvent(null, msg, Level.INFO, null);
		messages.append(msg + "<br/>");
	}

	public static void testGraph(String outputId) {
		messages.setLength(0);
		try {
			DFramedTransactionalGraph<DGraph> framedGraph = setupGraph();
			createMessage("Created demo data for...");
			createMessage("<ul>");
			Iterable<ConferenceSession> sessions = framedGraph.getVertices(null, null, ConferenceSession.class);
			for (ConferenceSession session : sessions) {
				createMessage("<li>" + session.getTitle() + "</li>");
			}
			createMessage("</ul>");

			ExtLibUtil.getViewScope().put(outputId, messages.toString());
		} catch (Throwable t) {
			XspOpenLogUtil.logError(t);
		}
	}

	public static void buildGraph() {
		messages.setLength(0);
		DFramedTransactionalGraph<DGraph> framedGraph = setupGraph();

		ConferenceSession session1 = framedGraph.addVertex(UUID.randomUUID().toString(), ConferenceSession.class);
		session1.setTitle("DDM, Letting Admins Sleep later and stay at Pubs longer since 2005");
		Presenter keith = framedGraph.addVertex("Keith Brooks", Presenter.class);
		keith.setName("Keith Brooks");
		session1.setPresenter(keith);

		ConferenceSession session2 = framedGraph.addVertex(UUID.randomUUID().toString(), ConferenceSession.class);
		session2.setTitle("Look mum, no passwords!");
		Presenter martin = framedGraph.addVertex("Martin Leyrer", Presenter.class);
		martin.setName("Martin Leyrer");
		session2.setPresenter(martin);

		ConferenceSession session3 = framedGraph.addVertex(UUID.randomUUID().toString(), ConferenceSession.class);
		session3.setTitle("Utilizing the OpenNTF Domino API in Domino Applications");
		Presenter oliver = framedGraph.addVertex("Oliver Busse", Presenter.class);
		oliver.setName("Oliver Busse");
		session3.setPresenter(oliver);

		// attendees
		Session session = Factory.getSession(SessionType.CURRENT);
		Name currentUser = session.createName(session.getEffectiveUserName());
		Attendee attCurrentUser = framedGraph.addVertex(currentUser.getCommon(), Attendee.class);
		attCurrentUser.setName(currentUser.getCommon());

		Attendee attPeter = framedGraph.addVertex("Peter Parker", Attendee.class);
		attPeter.setName("Peter Parker");

		Attendee attBruce = framedGraph.addVertex("Bruce Wayne", Attendee.class);
		attBruce.setName("Bruce Wayne");
		
		Attendee attClark = framedGraph.addVertex("Clark Kent", Attendee.class);
		attClark.setName("Clark Kent");

		session1.addAttendee(attCurrentUser);
		session2.addAttendee(attPeter);
		session3.addAttendee(attBruce);
		session3.addAttendee(attClark);

		framedGraph.commit();
	}

	public static List<ConferenceSession> getAllSessions() {
		DFramedTransactionalGraph<DGraph> framedGraph = setupGraph();
		Iterable<ConferenceSession> sessions = framedGraph.getVertices(null, null, ConferenceSession.class);
		List<ConferenceSession> allSessions = new ArrayList<ConferenceSession>();
		for (ConferenceSession sess : sessions) {
			allSessions.add(sess);
		}
		return allSessions;
	}

	public static List<Attendee> getAttendees(ConferenceSession sess) {
		List<Attendee> list = new ArrayList<Attendee>();
		try {
			for (Attendee att : sess.getAttendees()) {
				list.add(att);
			}
		} catch (Throwable t) {
			//			XspOpenLogUtil.logError(t);
		}
		return list;
	}

	public static String getPresenter(ConferenceSession sess) {
		try {
			Presenter presenter = sess.getPresenter();
			if (presenter != null) {
				return presenter.getName();
			}
		} catch (Throwable t) {
			//			XspOpenLogUtil.logError(t);
		}
		return "";
	}

	public static void saveSession() {
		try {
			Map<String, Object> viewScope = ExtLibUtil.getViewScope();
			DFramedTransactionalGraph<DGraph> framedGraph = setupGraph();
			ConferenceSession session = framedGraph.addVertex(UUID.randomUUID().toString(), ConferenceSession.class);
			session.setTitle(viewScope.get("sessionTitle").toString());
			Presenter presenter = framedGraph.addVertex(UUID.randomUUID().toString(), Presenter.class);
			presenter.setName(viewScope.get("sessionPresenter").toString());
			session.setPresenter(presenter);
			framedGraph.commit();
		} catch (Throwable t) {
			XspOpenLogUtil.logError(t);
		}
	}

	public static void saveAttendee(String key, String name) {
		try {
			DFramedTransactionalGraph<DGraph> framedGraph = setupGraph();
			Attendee att = framedGraph.addVertex(name, Attendee.class);
			att.setName(name.toString());
			ConferenceSession sess = framedGraph.getVertex(key, ConferenceSession.class);
			sess.addAttendee(att);
			framedGraph.commit();
		} catch (Throwable t) {
			XspOpenLogUtil.logError(t);
		}
	}

	public static boolean isAttending(ConferenceSession sess) {
		boolean result = false;
		try {
			Session session = Factory.getSession(SessionType.CURRENT);
			Name currentUser = session.createName(session.getEffectiveUserName());
			List<Attendee> attendees = getAttendees(sess);
			for (Attendee att : attendees) {
				if (att.getName().equals(currentUser.getCommon())) {
					return true;
				}
			}
		} catch (Throwable t) {
			XspOpenLogUtil.logError(t);
		}
		return result;
	}

	public static void addCurrentUserAsAttendee(String key) {
		try {
			Session session = Factory.getSession(SessionType.CURRENT);
			Name currentUser = session.createName(session.getEffectiveUserName());
			DFramedTransactionalGraph<DGraph> framedGraph = setupGraph();
			Attendee att = framedGraph.addVertex(currentUser.getCommon(), Attendee.class);
			att.setName(currentUser.getCommon());
			ConferenceSession sess = framedGraph.getVertex(key, ConferenceSession.class);
			sess.addAttendee(att);
			framedGraph.commit();
		} catch (Throwable t) {
			XspOpenLogUtil.logError(t);
		}
	}

	public static void unattendSession(String key) {
		try {
			Session session = Factory.getSession(SessionType.CURRENT);
			Name currentUser = session.createName(session.getEffectiveUserName());
			DFramedTransactionalGraph<DGraph> framedGraph = setupGraph();
			Attendee att = framedGraph.getVertex(currentUser.getCommon(), Attendee.class);
			ConferenceSession sess = framedGraph.getVertex(key, ConferenceSession.class);
			// TODO  this won't work due to an error in ODA - soon to be fixed
			sess.removeAttendee(att);
			framedGraph.commit();
		} catch (Throwable t) {
			XspOpenLogUtil.logError(t);
		}
	}

}
