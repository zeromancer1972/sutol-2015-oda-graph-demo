package org.sutol.demo.graph;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.faces.context.FacesContext;

import org.openntf.domino.xsp.XspOpenLogUtil;

public class GraphConfig {
	public static final String SESSIONS_PATH = getProperty("SESSIONS_PATH");
	public static final String PRESENTER_PATH = getProperty("PRESENTER_PATH");
	public static final String ATTENDEES_PATH = getProperty("ATTENDEES_PATH");

	private static Properties getDataSourceProperties() throws IOException {
		InputStream input = FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream("conference.properties");
		Properties props = new Properties();
		props.load(input);
		return props;
	}

	private static String getProperty(String prop) {
		try {
			return getDataSourceProperties().getProperty(prop);
		} catch (Throwable t) {
			XspOpenLogUtil.logError(t);
			return null;
		}
	}
}
