package org.openntf.bstemplate;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;

import org.openntf.domino.Database;
import org.openntf.domino.utils.XSPUtil;

public class AppConfig implements Serializable {

	private static final long serialVersionUID = 1815182743940537568L;

	private Map<String, ArrayList<String>> valueLists = null;

	public ArrayList<String> getList(final String key) {
		return valueLists.get(key);
	}

	public ArrayList<String> getSortedList(final String key) {
		ArrayList<String> list = valueLists.get(key);
		if (list != null) {
			Collections.sort(list);
		}
		return list;
	}

	public String getAppTitle() {
		return "SUTOL 2015 - OpenNTF Domino API Demo";
	}

	public String getAppIcon() {
		try {
			return valueLists.get("appIcon").get(0);
		} catch (Exception e) {
			return "fa fa-rebel";
		}
	}

	public String getString(final String key) {
		try {
			return valueLists.get(key).get(0);
		} catch (Exception e) {
			return null;
		}

	}

	public boolean isCreateDocuments() {
		int aclOptions = XSPUtil.getCurrentDatabase().queryAccessPrivileges(XSPUtil.getCurrentSession().getEffectiveUserName());
		return (aclOptions & Database.DBACL_CREATE_DOCS) > 0;
	}

}
