package org.openntf.bstemplate;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lotus.domino.Name;

import com.ibm.xsp.extlib.util.ExtLibUtil;

public class NavigationFooter implements Serializable {

	private static final long serialVersionUID = -8857086205056457935L;
	private List<Page> navigation;

	public NavigationFooter() {
		this.navigation = new ArrayList<Page>();
		AppConfig config = (AppConfig) ExtLibUtil.resolveVariable("application");

		try {
			// render if not logged in
			navigation.add(new Page("Login", "login.xsp", "", false, ExtLibUtil.getCurrentSession().getEffectiveUserName().equals("Anonymous")));

			// render if logged in
			Name uname = ExtLibUtil.getCurrentSession().createName(ExtLibUtil.getCurrentSession().getEffectiveUserName());
			navigation.add(new Page("Logout: " + uname.getAbbreviated(), "/" + ExtLibUtil.getCurrentDatabase().getFilePath() + "?logout&redirectto=" + "/"
					+ ExtLibUtil.getCurrentDatabase().getFilePath(), "", false, !ExtLibUtil.getCurrentSession().getEffectiveUserName().equals("Anonymous")));

			navigation.add(new Page("Help", "help.xsp", "", false, true));
			navigation.add(new Page("Setup", "setup.xsp", "", false, config.isCreateDocuments()));

		} catch (Exception e) {
			// nothing to do
		}
	}

	public List<Page> getNavigation() {
		return navigation;
	}
}
