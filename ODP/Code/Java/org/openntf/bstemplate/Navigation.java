package org.openntf.bstemplate;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Navigation implements Serializable {

	private static final long serialVersionUID = 8031965383531253276L;
	private final List<Page> navigation;
	

	public Navigation() {
		this.navigation = new ArrayList<Page>();
		this.navigation.add(new Page("Session Handling", "sessions.xsp", "", false));
		this.navigation.add(new Page("View Handling", "views.xsp", "", false));		
		this.navigation.add(new Page("Document Handling", "docs.xsp", "", false));
//		this.navigation.add(new Page("Document Listener", "doclistener.xsp", "", false));
		this.navigation.add(new Page("DateTime", "datetime.xsp", "", false));
		this.navigation.add(new Page("Transactions", "transactions.xsp", "", false));
		this.navigation.add(new Page("Xots", "xots.xsp", "", false));
		this.navigation.add(new Page("Graphs", "graphs.xsp", "", false));		
	}

	public List<Page> getNavigation() {
		return navigation;
	}
}
